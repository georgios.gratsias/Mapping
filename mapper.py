from abc import ABCMeta, abstractmethod
import argparse
import os
import gzip
import custom_logger as log
import re
import sys
if sys.version_info < (3, 2):
    import subprocess32 as sp
else:
    import subprocess as sp

## This is the abstract class to use when implementing a new mapper.
#  Try to minimize the output in the console, for example redirect the output of a call to dev/null or save it somewhere.
class Mapper:

    ## This is a meta class.
    __metaclass__ = ABCMeta

    @abstractmethod
    ## The constructor.
    #  @param query Path to the query file. Type: str for single-end reads, list of two str for paired-end reads.
    #  @param reference Path to the reference file. Type: str
    #  @param output Path to the output file without the file extension (for example '.sam'). Type: str
    #  @param options Contains all additional options. Type: list of str, organized like sys.argv
    #  @param logger Optional log. Type: logging.logger object
    #  In self.options may be given: A path where to save the index. Default: outputdir + timestamp
    #  for example:
    #      index_location: Expected path to the index, that is where it should be saved.
    #      Type: str
    #      index: Real path to the index.
    #          Type: str
    #
    #  and
    #  verbosity: May be given in self.options.
    #              1: Report steps.
    #      Type: int
    #
    #  You may add more initialization, for example detection of input file types (for example .fa or .fastq in Bowtie2).
    #
    #  self.report() at the end.
    def __init__(self, query, reference, output, options=None, logger=None):
        ## @var query
        #  The reads to map.
        self.query = query
        ## @var reference
        #  The reference to map against.
        self.reference = os.path.expanduser(reference)
        ## @var query_paired
        #  True if paired reads, false otherwise.
        self.query_paired = len(query) == 2 #isinstance(query, list)
        ## @var output
        #  Output file name without extension
        self.output = os.path.expanduser(output) # For example, without file extension
        if self.query_paired:
            self.query[0] = os.path.expanduser(self.query[0])
            self.query[1] = os.path.expanduser(self.query[1])
        else:
            self.query = os.path.expanduser(self.query[0])
        ## @var statistics
        #  Output of the mapper containing information about metrics.
        self.statistics = None
        ## @var version
        #  Version of the mapper.
        self.version = None
        ## @var index
        #  Index to use.
        self.index = None
        ## @var logger
        #  Logging object to write to.
        self.logger = logger
        ## @var verbosity
        #  Verbosity of output.
        self.verbosity = 1
        # parse options
        self.report()

    @abstractmethod
    ## Index and map.
    #  @param self The object pointer.
    #  Create index if non exists (self.index). Call mapper to map self.query to self.reference using additional options.
    #  Automatically detects single- or paired-end reads.
    #  Return the absolute path of the location of the generated SAM-file.
    #  Return None if an error occurs.
    #  If the console output contains statistics (for example in Bowtie2), these should be saved in self.statistics."""
    def map(self):
        return True

    @abstractmethod
    ## Return version of the mapper.
    #  @param self The object pointer.
    def get_version(self):
        return self.version

    @abstractmethod
    ## Write unmapped reads.
    #  @param self The object pointer.
    #  @param un basename of the file to write to.
    def write_unmapped(self, un):
        return True

    @abstractmethod
    ## Return collected statistics.
    #  @param self The object pointer.
    def get_statistics(self):
        return self.statistics

    @abstractmethod
    ## Remove the index.
    #  @param self The object pointer.
    #  You may want to use 'find' instead of 'rm'.
    def cleanup(self):
        return True

    @abstractmethod
    ## Write report.
    #  @param self The object pointer.
    #  Write all meaningful variables (at least the one defined in __init__) and their respective values to the logger.
    def report(self):
        return True

## Implementation of the Mapper class for Bowtie2.
class Bowtie2(Mapper):

    ## The constructor.
    #  @param self The object pointer.
    #  @param query The reads to map.
    #  @param reference The reference to map against.
    #  @param output The basename of the output file name.
    #  @param tmp Temporary dir to save index to.
    #  @param options Options to pass to the mapper.
    #  @param logger logging object to write to.
    #  @param readgroups Whether to set read groups.
    #  @param unmapped Filename without extension to write unmapped reads to.
    #  @param samtools Directory containing samtools executable.
    #  @param bowtie2 Directory containing bowtie2 executable.
    #  @param platform Used platform.
    #  @param threads Number of threads to use.
    def __init__(self, query, reference, output, tmp=None, options=None, logger=None, readgroups=True, unmapped=None, samtools=None, bowtie2=None, platform='ILLUMINA', threads=1):
        ## @var query
        #  Reads to map.
        self.query = query
        ## @var platform
        #  Used platform.
        self.platform = platform
        ## @var reference
        #  Reference to map against.
        self.reference = os.path.expanduser(reference)
        ## @var output
        #  The full filename of the output SAM file.
        self.output = os.path.expanduser('{}{}'.format(output,'.sam'))
        ## @var reference_fasta
        #  True if reference is a fasta file.
        self.reference_fasta = reference.endswith('.fa') or reference.endswith('.mfa')
        ## @var samtools
        #  Directory containing samtools executable.
        self.samtools = samtools
        ## @var bowtie2
        #  Directory containing bowtie2 executable.
        self.bowtie2 = bowtie2
        ## @var threads
        #  Number of threads to use.
        self.threads = threads
        ## @var query_paired
        #  True if reads are paired.
        self.query_paired = len(query) == 2 #isinstance(query, list)
        if self.query_paired:
            self.query[0] = os.path.expanduser(self.query[0])
            self.query[1] = os.path.expanduser(self.query[1])
        else:
            self.query = os.path.expanduser(self.query[0])
        ## @var query_fasta
        #  True if reads are in fasta format.
        self.query_fasta = query[0].endswith('.fa') or query[0].endswith('.mfa') #if self.query_paired else query.endswith('.fa') or query.endswith('.mfa')
        ## @var statistics
        #  Output of mapper to stdout.
        self.statistics = None
        ## @var version
        #  Version of bowtie2.
        self.version = None
        ## @var index
        #  Index to use.
        self.index = None
        ## @var verbosity
        #  Output verbosity.
        self.verbosity = 1
        ## @var logger.
        #  Logger to write to.
        self.logger = logger
        ## @var readgroups
        #  True if read groups are to be set.
        self.readgroups = readgroups
        ## @var options
        #  Options to run bowtie2 with.
        self.options = options
        ## @var unmapped
        #  Path without extension to write unmapped reads to.
        self.unmapped = unmapped
        if self.unmapped is not None:
            if self.query_paired:
                if self.options is not None:
                    self.options += ' --un-conc {}_{}'.format(self.unmapped, os.path.basename(self.query[0]))
                else:
                    self.options = ' --un-conc {}_{}'.format(self.unmapped, os.path.basename(self.query[0]))
            else:
                if self.options is not None:
                    self.options += ' --un {}_{}'.format(self.unmapped, os.path.basename(self.query))
                else:
                    self.options = ' --un {}_{}'.format(self.unmapped, os.path.basename(self.query))
        if tmp is None:
            ## @var index_location
            #  Exact location of index that can be passed to mapper.
            self.index_location = os.path.join(os.path.dirname(output), '.'.join(os.path.basename(reference).split('.')[:-1]))
        else:
            self.index_location = os.path.join(tmp, '.'.join(os.path.basename(reference).split('.')[:-1]))
        try:
            os.mkdir(os.path.expanduser('~/tmp/'))
        except OSError:
            pass
        self.report()
        #TODO options may change index_location or verbosity

    ## Update parameters of Mapper class to map another sample.
    #  @param self The object pointer.
    #  @param query The reads.
    #  @param output The filename of the output without extension.
    #  @param options Options to use for mapping.
    def update(self, query, output, options):
        self.query = query
        self.output = os.path.expanduser('{}{}'.format(output,'.sam'))
        self.query_paired = len(query) == 2 #isinstance(query, list)
        if self.query_paired:
            self.query[0] = os.path.expanduser(self.query[0])
            self.query[1] = os.path.expanduser(self.query[1])
            if self.unmapped is not None:
                if options is not None:
                    self.options = options + ' --un-conc {}_{}'.format(self.unmapped, os.path.basename(self.query[0]))
                else:
                    self.options = ' --un-conc {}_{}'.format(self.unmapped, os.path.basename(self.query[0]))
        else:
            self.query = os.path.expanduser(self.query[0])
            if self.unmapped is not None:
                if options is not None:
                    self.options = options + ' --un {}_{}'.format(self.unmapped, os.path.basename(self.query))
                else:
                    self.options = ' --un {}_{}'.format(self.unmapped, os.path.basename(self.query))
        self.query_fasta = query[0].endswith('.fa') or query[0].endswith('.mfa') #if self.query_paired else query.endswith('.fa') or query.endswith('.mfa')

    ## Create an index from given reference.
    #  @param self The object pointer.
    def create_index(self):
        if self.options is not None and '-x' in self.options:
            self.index = None
        else:
            try:
                if self.verbosity >= 1:
                    log.print_write(self.logger, 'info', '=== Building index ===')
                try:
                    if self.reference == '':
                        raise Exception('No reference given.')
                except:
                    log.print_write(self.logger, 'exception', 'ERROR: No reference given.')
                    sys.exit(1)
                ## @var command
                #  Temporarly store command call.
                if not self.bowtie2:
                    build_cmd = 'bowtie2-build'
                else:
                    build_cmd = os.path.realpath(os.path.join(self.bowtie2,
                                                              'bowtie2-build'))

                self.command = [
                        x for x
                        in [build_cmd,
                            '-f' if self.reference_fasta else None,
                            self.reference,
                            self.index_location]
                        if x is not None]
                log.call(self.command, self.logger)
            except sp.CalledProcessError:
                log.print_write(self.logger, 'exception', 'ERROR: CPE in Bowtie2.create_index()')
                return False
            self.index = self.index_location

    ## Map reads against index.
    #  @param self The object pointer.
    def map(self):
        if self.index is None:
            self.create_index()
        try:
            if self.readgroups:
                if self.query_paired:
                    m = os.path.basename(self.query[0])
                else:
                    m = os.path.basename(self.query)
                try: # Raw
                    rg_sm = re.match("(.*)_L[\d]+_R[\d]+(_[\d]+){0,1}.*", m).group(1)
                except AttributeError:
                    try: # QCumber
                        rg_sm = re.match("(.*?)(\.[1,2][U,P]){0,1}\..*", m).group(1)
                    except AttributeError:
                        rg_sm = m.split('.')[0]
                rg_pl = self.platform
                rg_id = rg_sm
                rg_pu = rg_id
                rg_lb = 'lib_' + rg_sm
            else:
                rg_sm = None
                rg_pl = None
                rg_id = None
                rg_pu = None
                rg_lb = None

            if self.verbosity >= 1:
                log.print_write(self.logger, 'info', '=== Mapping query to reference ===')
            if not self.bowtie2:
                bowtie_path = 'bowtie2'
            else:
                os.path.realpath(os.path.join(self.bowtie2, 'bowtie2'))
            self.command = [x for x in ([
                    bowtie_path,
                    '-f' if self.query_fasta else None,
                    '-x' if self.index else None,
                    self.index if self.index else None,
                    '-1' if self.query_paired else None,
                    self.query[0] if self.query_paired else None,
                    '-2' if self.query_paired else None,
                    self.query[1] if self.query_paired else None,
                    '-U' if not self.query_paired else None,
                    self.query if not self.query_paired else None,
                    '-S', self.output,
                    '-p', str(self.threads),
                    '--rg-id' if self.readgroups else None, rg_id if self.readgroups else None,
                    '--rg' if self.readgroups else None, 'SM:{}'.format(rg_sm) if self.readgroups else None,
                    '--rg' if self.readgroups else None, 'PU:{}'.format(rg_pu) if self.readgroups else None,
                    '--rg' if self.readgroups else None, 'PL:{}'.format(rg_pl) if self.readgroups else None,
                    '--rg' if self.readgroups else None, 'LB:{}'.format(rg_lb) if self.readgroups else None]
                    + (self.options.split(' ') if self.options else list())
                    )
                if x is not None] # TODO options
            #print("DEBUG: " + ' '.join(self.command))
            self.statistics = log.check_output(self.command, self.logger, stderr=sp.STDOUT)
            return self.output
        except sp.CalledProcessError:
            log.print_write(self.logger, 'exception', 'ERROR: CPE in Bowtie2.map()')
            return None

    ## Set version string of mapper.
    #  @param self The object pointer.
    def set_version(self):
        try:
            if not self.bowtie2:
                self.command = ['bowtie2', '--version']
            else:
                self.command = [
                    os.path.realpath(os.path.join(self.bowtie2, 'bowtie2')),
                    '--version']
            self.version = re.match('.*version (.*)\n', str(sp.check_output(self.command), 'utf-8')).group(1)
        except sp.CalledProcessError:
            log.write(self.logger, 'exception', 'ERROR: CPE in Bowtie2.set_version')
            return False

    ## Get version string of mapper.
    #  @param self The object pointer.
    def get_version(self):
        if self.version is None:
            self.set_version()
        return self.version

    ## Return collected mapper output.
    #  @param self The object pointer.
    def get_statistics(self):
        return self.statistics

    ## Return options for fast mode.
    #  @param self The object pointer.
    def get_fast_options(self):
        return '-k 10'

    ## Return options for sensitive mode.
    #  @param self The object pointer.
    def get_sensitive_options(self):
        return '-a'

    ## Delete index if it was not given via options.
    #  @param self The object pointer.
    def cleanup(self):
        if self.index is not None:
            if self.verbosity >= 1:
                log.write(self.logger, 'info', '=== Cleaning up mapper ===')
            try:
                self.command = ('find {} -type f -regextype posix-extended '
                        '-iregex \'.*{}\.(([0-9]+\.bt2)|(rev\.[0-9]+\.bt2)|fasta)$\' -delete'.format(os.path.dirname(self.index), os.path.basename(self.index)))
                log.call(self.command, self.logger, shell=True)
                self.index = None
            except sp.CalledProcessError:
                log.print_write(self.logger, 'exception', 'ERROR: CPE in Bowtie2.cleanup()')
                return False

    ## Report version and parameters for call.
    #  @param self The object pointer.
    def report(self):
        log.write(self.logger, 'warning', 'Bowtie2 Version: {}'.format(self.get_version()))
        log.write(self.logger, 'warning', 'Parameters: ')
        for arg in sorted(vars(self)):
            if arg != 'command':
                log.write(self.logger, 'warning', '{} = {}'.format(arg, getattr(self, arg)))

## Implementation of the Mapper class for BWAMem.
class BWAMem(Mapper):

    ## The constructor.
    #  @param self The object pointer.
    #  @param query The reads to map.
    #  @param reference The reference to map against.
    #  @param output The basename of the output file name.
    #  @param tmp Temporary dir to save index to.
    #  @param options Options to pass to the mapper.
    #  @param logger logging object to write to.
    #  @param readgroups Whether to set read groups.
    #  @param unmapped Filename without extension to write unmapped reads to.
    #  @param samtools Directory containing samtools executable.
    #  @param bwa Directory containing bwa executable.
    #  @param platform Used platform.
    #  @param threads Number of threads to use.
    def __init__(self, query, reference, output, tmp=None, options=None, logger=None, readgroups=True, unmapped=None, samtools=None, bwa=None, platform='ILLUMINA', threads=1):
        ## @var query
        #  Reads to map.
        self.query = query
        ## @var platform
        #  Used platform.
        self.platform = platform
        ## @var reference
        #  Reference to map against.
        self.reference = os.path.expanduser(reference)
        ## @var query_paired
        #  True if reads are paired.
        self.query_paired = len(query) == 2 #isinstance(query, list)
        ## @var output
        #  The filename of the output SAM file without extension.
        self.output = os.path.expanduser('{}.sam'.format(output))
        ## @var samtools
        #  Directory containing samtools executable.
        self.samtools = samtools
        ## @var bwa
        #  Directory containing bwa executable.
        self.bwa = bwa
        ## @var unmapped_raw
        #  Temporarly storage path where unmapped reads are written to.
        self.unmapped_raw = unmapped
        ## @var unmapped
        #  Path without extension to write unmapped reads to.
        self.unmapped = ''
        ## @var threads
        #  Number of threads to use.
        self.threads = threads
        if self.query_paired:
            self.query[0] = os.path.expanduser(self.query[0])
            self.query[1] = os.path.expanduser(self.query[1])
            self.unmapped = '{}_{}'.format(self.unmapped_raw, os.path.splitext(os.path.basename(self.query[0]))[0])
        else:
            self.query = os.path.expanduser(self.query[0])
            self.unmapped = '{}_{}'.format(self.unmapped_raw, os.path.splitext(os.path.basename(self.query))[0])
        if self.unmapped_raw is None:
            self.unmapped = None
        ## @var statistics
        #  Output of mapper to stdout.
        self.statistics = None
        ## @var version
        #  Version of bwa.
        self.version = None
        ## @var index
        #  Index to use.
        self.index = None
        ## @var options
        #  Options to run bwa with.
        self.options = options
        ## @var index_given
        #  Whether the index is given via the -x option. This options needs then to be removed from the options string since -x is not supported by bwa.
        self.index_given = '-x' in self.options if self.options is not None else False
        ## @var logger.
        #  Logger to write to.
        self.logger = logger
        ## @var verbosity
        #  Output verbosity.
        self.verbosity = 1
        ## @var readgroups
        #  True if read groups are to be set.
        self.readgroups = readgroups
        if tmp is None:
            ## @var index_location
            #  The exact path to the index that can be passed to the indexer and mapper.
            self.index_location = os.path.join(os.path.dirname(output), '.'.join(os.path.basename(reference).split('.')[:-1]))
        else:
            self.index_location = os.path.join(tmp, '.'.join(os.path.basename(reference).split('.')[:-1]))
        self.report()

    ## Update parameters of Mapper class to map another sample.
    #  @param self The object pointer.
    #  @param query The reads.
    #  @param output The filename of the output without extension.
    #  @param options Options to use for mapping.
    def update(self, query, output, options):
        self.query = query
        self.output = os.path.expanduser('{}{}'.format(output,'.sam'))
        self.query_paired = len(query) == 2 #isinstance(query, list)
        if self.query_paired:
            self.query[0] = os.path.expanduser(self.query[0])
            self.query[1] = os.path.expanduser(self.query[1])
            if self.unmapped_raw is not None:
                self.unmapped = '{}_{}'.format(self.unmapped_raw, os.path.splitext(os.path.basename(self.query[0]))[0])
        else:
            self.query = os.path.expanduser(self.query[0])
            if self.unmapped_raw is not None:
                self.unmapped = '{}_{}'.format(self.unmapped_raw, os.path.splitext(os.path.basename(self.query))[0])
        ## @var query_fasta
        #  True if the reads are in fasta format.
        self.query_fasta = query[0].endswith('.fa') or query[0].endswith('.mfa') #if self.query_paired else query.endswith('.fa') or query.endswith('.mfa')

    ## Create an index from given reference.
    #  @param self The object pointer.
    def create_index(self):
        # If -x is given, we set the reference to this value and remove -x from the options string.
        if self.options is not None and '-x' in self.options:
            cache = self.options.split(' ')
            pos = cache.index('-x')
            self.index_location = cache[pos + 1]
            del cache[pos:pos+2]
            self.options = ' '.join(cache)
        else:
            try:
                if self.verbosity >= 1:
                    log.print_write(self.logger, 'info', '=== Building index ===')
                try:
                    if self.reference == '':
                        raise Exception('No reference given.')
                except:
                    log.print_write(self.logger, 'exception', 'ERROR: No reference given.')
                    sys.exit(1)
                ## @var command
                #  Temporarly store command call.
                if not self.bwa:
                    build_cmd = 'bwa'
                else:
                    build_cmd = os.path.realpath(os.path.join(self.bwa,
                                                              'bwa'))
                self.command = [build_cmd, 'index', '-p', self.index_location, self.reference]
                log.call(self.command, self.logger)
            except sp.CalledProcessError:
                log.print_write(self.logger, 'exception', 'ERROR: CPE in BWAMem.create_index()')
                return False
        self.index = self.index_location

    ## Map reads against index.
    #  @param self The object pointer.
    def map(self):
        if self.index is None:
            self.create_index()
        try:
            if self.readgroups:
                if self.query_paired:
                    m = os.path.basename(self.query[0])
                else:
                    m = os.path.basename(self.query)
                try: # Raw
                    rg_sm = re.match("(.*)_L[\d]+_R[\d]+(_[\d]+){0,1}.*", m).group(1)
                except AttributeError:
                    try: # QCumber
                        rg_sm = re.match("(.*?)(\.[1,2][U,P]){0,1}\..*", m).group(1)
                    except AttributeError:
                        rg_sm = m.split('.')[0]
                rg_pl = self.platform
                rg_id = rg_sm
                rg_pu = rg_id
                rg_lb = 'lib_' + rg_sm
            else:
                rg_sm = None
                rg_pl = None
                rg_id = None
                rg_pu = None
                rg_lb = None

            if self.verbosity >= 1:
                log.print_write(self.logger, 'info', '=== Mapping query to reference ===')
            if self.bwa:
                bwa_path = os.path.realpath(os.path.join(self.bwa, 'bwa'))
            else:
                bwa_path = 'bwa'
            self.command = ' '.join([x for x in [
                bwa_path,
                'mem',
                self.index,
                self.query[0] if self.query_paired else None,
                self.query[1] if self.query_paired else None,
                self.query if not self.query_paired else None,
                '-R' if self.readgroups else None, '\"@RG\\tID:{}\\tPU:{}\\tSM:{}\\tPL:{}\\tLB:{}\"'.format(rg_id, rg_pu, rg_sm, rg_pl, rg_lb) if self.readgroups else None,
                '-t', str(self.threads),
                self.options if self.options else None,
                '>', self.output
                ] if x is not None]) # TODO options
            self.statistics = log.check_output(self.command, self.logger, shell=True, stderr=sp.STDOUT)
            if self.samtools:
                samtools_path = os.path.realpath(os.path.join(self.samtools,
                                                              'samtools'))
            else:
                samtools_path = 'samtools'
            if self.unmapped is not None:
                self.command = ('{samtools} view -f4 {output} '
                                '| {samtools} view -Sb - '
                                '| {samtools} fastq -'
                                ' -1 {unmapped}.1.fastq -2 {unmapped}.2.fastq'
                                ' -s {unmapped}.singleton.fastq'
                                ' -0 {unmapped}.unpaired.fastq'.format(
                                    output=self.output,
                                    unmapped=self.unmapped,
                                    samtools=samtools_path))
                log.call(self.command, self.logger, shell=True)
                try:
                    if os.path.getsize('{}.1.fastq'.format(self.unmapped)) == 0:
                        os.remove('{}.1.fastq'.format(self.unmapped))
                    if os.path.getsize('{}.2.fastq'.format(self.unmapped)) == 0:
                        os.remove('{}.2.fastq'.format(self.unmapped))
                    if os.path.getsize('{}.singleton.fastq'.format(self.unmapped)) == 0:
                        os.remove('{}.singleton.fastq'.format(self.unmapped))
                    if os.path.getsize('{}.unpaired.fastq'.format(self.unmapped)) == 0:
                        os.remove('{}.unpaired.fastq'.format(self.unmapped))
                except:
                    pass
            return self.output
        except sp.CalledProcessError:
            log.print_write(self.logger, 'exception', 'ERROR: CPE in BWAMem.map()')
            return None

    ## Set version string of mapper.
    #  @param self The object pointer.
    def set_version(self):
        try:
            if not self.bwa:
                self.command = ['bwa']
            else:
                self.command = [
                    os.path.realpath(os.path.join(self.bwa, 'bwa'))]
            p = re.compile('.*Version: (.*?)\n', re.DOTALL)
            self.version = re.match(p, str(sp.Popen(self.command,stdout=sp.PIPE,stderr=sp.STDOUT).communicate()[0], 'utf-8')).group(1)
        except sp.CalledProcessError as e:
            log.write(self.logger, 'exception', 'ERROR: CPE in BWAMem.set_version')
            return False

    ## Get version string of mapper.
    #  @param self The object pointer.
    def get_version(self):
        if self.version is None:
            self.set_version()
        return self.version

    ## Return collected mapper output.
    #  @param self The object pointer.
    def get_statistics(self):
        return self.statistics

    ## Delete index if it was not given via options.
    #  @param self The object pointer.
    def cleanup(self):
        if self.index is not None and not self.index_given:
            if self.verbosity >= 1:
                log.write(self.logger, 'info', '=== Cleaning up mapper===')
            try:
                self.command = ('find {} -type f -regextype posix-extended '
                        '-iregex \'.*{}\.(amb|ann|bwt|pac|sa|fasta)$\' -delete'.format(os.path.dirname(self.index), os.path.basename(self.index)))
                log.call(self.command, self.logger, shell=True)
                self.index = None
            except sp.CalledProcessError:
                log.print_write(self.logger, 'exception', 'ERROR: CPE in BWAMem.cleanup()')
                return False

    ## Report version and parameters for call.
    #  @param self The object pointer.
    def report(self):
        log.write(self.logger, 'warning', 'BWA Version: {}'.format(self.get_version()))
        log.write(self.logger, 'warning', 'Parameters: ')
        for arg in sorted(vars(self)):
            if arg != 'command':
                log.write(self.logger, 'warning', '{} = {}'.format(arg, getattr(self, arg)))

# add new mappers here
## Return list of supported mappers.
def list_mapper():
    return ('bowtie2', 'bwa-mem')

# add new mappers here
## Create an instance of the given mapper.
#  @param mapper_key One of the mappers defined in list_mapper().
#  @param query The reads to map.
#  @param reference The reference to map against.
#  @param output The basename of the output file name.
#  @param tmp Temporary dir to save index to.
#  @param options Options to pass to the mapper.
#  @param logger logging object to write to.
#  @param readgroups Whether to set read groups.
#  @param unmapped Filename without extension to write unmapped reads to.
#  @param samtools Directory containing samtools executable.
#  @param bwa Directory containing bwa executable.
#  @param bowtie2 Directory containing bowtie2 executable.
#  @param platform Used platform.
#  @param threads Number of threads to use.
def get_mapper(mapper_key, query, reference, output, tmp=None, options=None, logger=None, readgroups=True, unmapped=None, samtools=None, bwa=None, bowtie2=None, platform='ILLUMINA', threads=1):
    if mapper_key == 'bowtie2':
        return Bowtie2(query, reference, output, tmp=tmp, options=options, logger=logger, readgroups=readgroups, unmapped=unmapped, samtools=samtools, bowtie2=bowtie2, platform=platform, threads=threads)
    if mapper_key == 'bwa-mem':
        return BWAMem(query, reference, output, tmp=tmp, options=options, logger=logger, readgroups=readgroups, unmapped=unmapped, samtools=samtools, bwa=bwa, platform=platform, threads=threads)
