#!/usr/bin/env python
import custom_logger as log
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
import os
import pysam
import pylab
import itertools
import json
import string
from scipy import stats
from collections import Counter
from collections import deque
from itertools import islice
import subprocess as sp

## Sliding window iterator.
#  @param iterable Iterable object to slide over.
#  @param size Size of the sliding window.
#  @param step Step size to slide.
#  @param fillvalue Value to fill iterator with.
def window(iterable, size=2, step=1, fillvalue=None):
    if size < 0 or step < 1:
        raise ValueError
    it = iter(iterable)
    q = deque(islice(it, size), maxlen=size)
    if not q:
        return  # empty iterable or size == 0
    #q.extend(fillvalue for _ in range(size - len(q)))  # pad to size
    while True:
        #yield iter(q)  # iter() to avoid accidental outside modifications
        yield q
        q.append(next(it))
        #q.extend(next(it, fillvalue) for _ in range(step - 1))
        q.extend(next(it) for _ in range(step - 1))

## Replace all invalid characters in a string to create a valid filename.
#  @param s String to convert.
def valid_filename(s):
    valid_chars = "-_.() %s%s" % (string.ascii_letters, string.digits)
    filename = ''.join(c for c in s if c in valid_chars)
    filename = filename.replace(' ','_')
    if len(filename) > 200:
        return(filename[:200])
    else:
        return filename

## Representation of a SAM file.
class SAM:

    ## The constructor.
    #  @param self The object pointer.
    #  @param sam_file_path File path of the SAM file.
    #  @param logger Logging object to write to.
    #  @param samtools Directory containing samtools executable.
    def __init__(self, sam_file_path, logger=None, samtools=None):
        ## @var samtools
        #  Directory containing samtools executable.
        self.samtools = samtools
        ## @var sam_file_path
        #  File path of the SAM file.
        self.sam_file_path = sam_file_path
        ## @var sam_file
        #  Pysam object of the SAM file.
        self.sam_file = None
        ## @var logger
        #  Logging object to write to.
        self.logger = logger
        #self.sam_alignments = list()
        self._read_sam()
        ## @var sam_flags
        #  List of flags for a SAM file.
        self.sam_flags = None

    ## Read a SAM file into a pysam object.
    #  @param self The object pointer.
    def _read_sam(self):
        sp.check_call('{samtools} view -bS {sam_file} | {samtools} sort - -o {bam} 2> /dev/null'.format(samtools=self.samtools, sam_file=self.sam_file_path, bam=self.sam_file_path[:-3] + 'bam'), shell=True)
        self.sam_file_path = self.sam_file_path[:-3] + 'bam'
        sp.check_call('{samtools} index {sam_file} {index}'.format(samtools=self.samtools, sam_file=self.sam_file_path, index=self.sam_file_path[:-1] + 'i'), shell=True)
        self.sam_file = pysam.AlignmentFile(self.sam_file_path, 'rb')
        #for alignment in self.sam_file.fetch():
        #    self.sam_alignments.append(alignment)
        #TODO return new iterator?
        #self.sam_alignments = self.sam_file.fetch()

    ## Read the flags in a SAM file.
    #  @param self The object pointer.
    def _read_flags(self):
        self.sam_flags = list()
        for self.alignment in self.get_alignments():
            self.sam_flags.append((self.alignment.query_name, self.alignment.flag))

    ## Get alignments in a SAM file.
    #  @param self The object pointer.
    #  @param reference Reference for the SAM file.
    #  @param until_eof Whether to get unaligned reads.
    def get_alignments(self, reference=None, until_eof=False):
        return self.sam_file.fetch(reference = reference, multiple_iterators = True, until_eof=until_eof)

    ## Get flags in a SAM file.
    #  @param self The object pointer.
    def get_flags(self):
        for self.alignment in self.get_alignments(until_eof=True):
            yield self.alignment.flag

    ## Return pysam object.
    #  @param self The object pointer.
    def get_sam_file(self):
        return self.sam_file

    ## Return file path of SAM file.
    #  @param self The object pointer.
    def get_sam_file_path(self):
        return self.sam_file_path

    ## Get number of references in SAM file.
    #  @param self The object pointer.
    def get_reference_count(self):
        return self.sam_file.nreferences

    ## Get name of a reference.
    #  @param self The object pointer.
    #  @param ref_id ID of the reference.
    def get_reference_name(self, ref_id):
        return self.sam_file.getrname(ref_id)

    ## Get lengths of references.
    #  @param self The object pointer.
    def get_lengths(self):
        return self.sam_file.lengths

    ## Get references.
    #  @param self The object pointer.
    def get_references(self):
        return self.sam_file.references

    ## Get index of reference in pysam references list.
    #  @param self The object pointer.
    #  @param reference Reference to look up.
    def get_reference_pos(self, reference):
        return self.sam_file.references.index(reference)

    ## Get length of a specific reference.
    #  @param self The object pointer.
    #  @param reference Reference to look up.
    def get_reference_length(self, reference):
        return self.sam_file.lengths[self.get_reference_pos(reference)]

## Representation of the metrics
class METRICS:

    ## The constructor
    #  @param self The object pointer.
    #  @param sam_file_path File path to the SAM file.
    #  @param output Path to the output directory.
    #  @param logger Logging object to write to.
    #  @param samtools Directory containing samtools executable.
    def __init__(self, sam_file_path, output=None, logger=None, samtools=None):
        ## @var sam
        #  Instance of the SAM class for a SAM file.
        self.sam = SAM(sam_file_path, logger, samtools)
        ## @var logger
        #  Logging object to write to.
        self.logger = logger
        if output is not None:
            ## @var output
            #  Absolute path to output directory.
            self.output = os.path.realpath(output)
        else:
            self.output = os.path.realpath('./')
        ## @var reference_coverage_count
        #  Stores the coverage at each position for one reference.
        self.reference_coverage_count = None
        ## @var reference_mapq_avg
        #  Stoes the average mapping quality at each position for one reference.
        self.reference_mapq_avg = None
        ## @var reads_per_quality_result
        #  Stores the reads per quality for each reference.
        self.reads_per_quality_result = []
        ## @var bases_per_coverage_result
        #  Stores the bases per coverage for each reference.
        self.bases_per_coverage_result = []
        ## @var mapq_graph_result
        #  Stores the filename of the mapping quality graph for each reference.
        self.mapq_graph_result = []
        ## @var coverage_graph_result
        #  Stores the filename of the coverage graph for each reference.
        self.coverage_graph_result = []
        ## @var cov_stats_result
        #  Stores the coverage stats for each reference.
        self.cov_stats_result = []
        #self.read_stats_result = []

    ## Generate one pdf for each reference showing the number of reads (y-axis) that map with a certain mapping quality (x-axis).
    #  @param self The object pointer.
    #  @param reference The reference for which to count the reads per quality.
    #  @param i Counter for the references.
    def reads_per_quality(self, reference, i):
        log.print_write(self.logger, 'info', '=== Reads per Quality #{} ==='.format(i))
        # The SAM convention states that 255 is the highest value for the mapping quality.
        reads_per_quality = np.zeros(256, np.int)
        # Go through all alignments int the SAM file and increase the counter for the mapping quality in the respective reference.
        for alignment in self.sam.get_alignments(reference=reference):
            reads_per_quality[alignment.mapping_quality] += 1
        # Trim all zeros at the end of the array.
        rpq = np.trim_zeros(reads_per_quality, 'b')
        # If there is something left to plot.
        if rpq.size > 0:
            # Plot the reads per quality as a bar chart into a pdf file.
            with PdfPages(os.path.join(self.output, 'reads_per_quality_{}.pdf'.format(valid_filename(reference)))) as pdf:
                self.reads_per_quality_result.append([os.path.realpath(os.path.join(self.output, 'reads_per_quality_{}.pdf'.format(valid_filename(reference)))), reference.replace('_','\_')])
                fig, ax = plt.subplots( nrows=1, ncols=1 )  # create figure & 1 axis
                ax.set_yscale('symlog')
                ax.bar(range(rpq.size), rpq, align='center')
                pylab.xlim([-0.5,rpq.size-0.5])
                pylab.ylim(ymin=-0.05)
                ax.set_xlabel('Mapping quality')
                ax.set_ylabel('Read Count')
                ax.set_title('Reads per Mapping Quality')
                pdf.savefig()  # saves the current figure into a pdf page
                plt.close()

    ## Calls functions that computes metrics for each reference.
    #  @param self The object pointer.
    def compute_metrics(self):
        scored_references = list()
        score_reference = np.zeros(self.sam.get_reference_count(), np.int)
        for alignment in self.sam.get_alignments():
            score_reference[alignment.reference_id] += 1
        scored_references = [self.sam.get_references()[x] for x in np.argsort(score_reference)[::-1][:10]]
        for i, reference in enumerate(scored_references):
            if i == 10:
                break
            self.count_coverage(reference)
            self.count_mapq(reference)
            self.reads_per_quality(reference, i+1)
            self.bases_per_coverage(reference, i+1)
            self.coverage_graph(reference, i+1)
            self.mapq_graph(reference, i+1)
            self.coverage_stats(reference, i+1)

    ## Count coverage for each position in each reference, that is reference_count[reference_id][position] = coverage.
    #  @param self The object pointer.
    #  @param reference The reference to count for.
    def count_coverage(self, reference):
        # We will count coverages for all references(self.sam.lengths). One row will contain at most the max(lengths references) entries.
        self.reference_coverage_count = np.zeros(self.sam.get_reference_length(reference), np.int)
        # Go through all alignments in the SAM file.
        for alignment in self.sam.get_alignments(reference=reference):
            # If the length is None, the query/read does not map anywhere.
            if alignment.reference_length is not None:
                # Increase the coverage by one for the positions in the reference that the read maps to.
                self.reference_coverage_count[alignment.reference_start:alignment.reference_end] += 1

    ## Append mapping quality for each position in each reference, that is reference_count[reference_id][position] = [mapqs].
    #  @param self The object pointer.
    #  @param reference The reference to count for.
    def count_mapq(self, reference):
        # We will count coverages for all references(self.sam.lengths). One row will contain at most the max(lengths references) entries.
        reference_mapq_sum = np.zeros(self.sam.get_reference_length(reference), np.float)
        reference_mapq_count = np.zeros(self.sam.get_reference_length(reference), np.int)
        # Go through all alignments in the SAM file.
        for alignment in self.sam.get_alignments(reference=reference):
            # If the length is None, the query/read does not map anywhere.
            if alignment.reference_length is not None:
                # Increase the coverage by one for the positions in the reference that the read maps to.
                mapq = alignment.mapping_quality
                reference_mapq_sum[alignment.reference_start:alignment.reference_end] += mapq
                reference_mapq_count[alignment.reference_start:alignment.reference_end] += 1
        with np.errstate(divide='ignore', invalid='ignore'):
            self.reference_mapq_avg = np.true_divide(reference_mapq_sum, reference_mapq_count)
            self.reference_mapq_avg = np.nan_to_num(self.reference_mapq_avg)
            self.reference_mapq_avg[self.reference_mapq_avg == np.inf] = 0

    ## Calculate float mean of the values in array and return the rounded float as int.
    #  @param self The object pointer.
    #  @param array Array to use.
    def _integer_mean(self, array):
        if len(array) == 0:
            return 0
        return int(round(np.sum(array)/len(array)))

    ## Manipulate the xlabel for a plot.
    #  @param self The object pointer.
    #  @param ax Axis object.
    #  @param factor Factor to multiply axis labels with.
    def _set_axis(self, ax, factor):
        # The new label should be the "compression" factor caused by the binning * the position.
        def _factor_label(x, pos):
            return str(int(round(x * factor)))
        # Create a new formatter using _factor_label
        formatter = matplotlib.ticker.FuncFormatter(_factor_label)
        # Set the formatter
        ax.xaxis.set_major_formatter(formatter)

    ## Generate one pdf for each reference showing the number of bases (y-axis) that have a certain coverage (x-axis).
    #  @param self The object pointer.
    #  @param reference Reference to count for.
    #  @param i Reference counter.
    def bases_per_coverage(self, reference, i):
        log.print_write(self.logger, 'info', '=== Bases per Coverage #{} ==='.format(i))
        # If we have not count the coverages yet, have to count the coverages.
        if self.reference_coverage_count is None:
            self.count_coverage(reference)
        # Trim coverages of 0 at the end.
        reference_coverage = np.trim_zeros(self.reference_coverage_count, 'b')
        # If there are some coverages left.
        if reference_coverage.size > 0:
            # Count how often a coverage appears. bincount[coverage]=#coverage
            bases_per_coverage = np.bincount(reference_coverage)
            # Bin the coverages into 1000 bin and calculate the mean for every bin.
            binned_coverage = stats.binned_statistic(range(bases_per_coverage.size), bases_per_coverage, statistic = self._integer_mean, bins = 1000)[0]
            # Trim again in case the maximal coverage is less than 1000.
            binned_coverage = np.trim_zeros(binned_coverage, 'b')
            # Get the number of binned coverages.
            len_coverage = binned_coverage.size
            # Plot the bases per coverage as a bar chart into a pdf file.
            if len_coverage > 0:
                with PdfPages(os.path.join(self.output, 'bases_per_coverage_{}.pdf'.format(valid_filename(reference)))) as pdf:
                    self.bases_per_coverage_result.append([os.path.realpath(os.path.join(self.output, 'bases_per_coverage_{}.pdf'.format(valid_filename(reference)))), reference.replace('_','\_')])
                    fig, ax = plt.subplots(nrows=1, ncols=1)
                    # Change labels if there are more unbinned coverages than binned coverages, for example the binning influences the labels.
                    if len_coverage <= bases_per_coverage.size:
                        factor = float(bases_per_coverage.size)/len_coverage
                        self._set_axis(ax, factor)
                    ax.set_yscale('symlog')
                    ax.bar(range(len_coverage), binned_coverage, align='center', color='b', linewidth=0, edgecolor='b')
                    pylab.xlim([-0.005*len_coverage, len_coverage+0.005*len_coverage])
                    ax.set_xlabel('Coverage')
                    ax.set_ylabel('Base Count')
                    ax.set_title('Bases per Coverage')
                    pylab.ylim(ymin=-0.05)
                    pdf.savefig()
                    plt.close()
        if len(self.bases_per_coverage_result) == 0:
            self.bases_per_coverage_result.append(None)

    ## Generate one pdf for each reference showing the genome position (y-axis) an the respective coverage (x-axis). The genome is reduced/binned into 1000 points.
    #  @param self The object pointer.
    #  @param reference Reference to count for.
    #  @param i Reference counter.
    def coverage_graph(self, reference, i):
        log.print_write(self.logger, 'info', '=== Coverage Graph #{} ==='.format(i))
        if self.reference_coverage_count is None:
            self.count_coverage(reference)
        # Trim coverages of 0 at the end.
        reference_coverage = np.trim_zeros(self.reference_coverage_count, 'b')
        # If there is something left of the reference.
        if reference_coverage.size > 0:
            # Use a sliding window to create ~1000 datapoints out of the reference. 
            reference_coverage_mean = []
            reference_coverage_max = []
            reference_coverage_min = []
            window_size = min(int(reference_coverage.size/10), 50000)
            for w in window(reference_coverage, size=window_size, step=max(1, int((reference_coverage.size-window_size)/1000))):
                reference_coverage_mean.append(int(np.mean(w)))
                reference_coverage_max.append(max(w))
                reference_coverage_min.append(min(w))
            reference_coverage_mean = np.array(reference_coverage_mean)
            reference_coverage_max = np.array(reference_coverage_max)
            reference_coverage_min = np.array(reference_coverage_min)
            # Get the number of binned reference.
            len_reference = reference_coverage_mean.size
            # Plot bases per coverage as line diagram.
            if len_reference > 0:
                with PdfPages(os.path.join(self.output, 'coverage_graph_{}.pdf'.format(valid_filename(reference)))) as pdf:
                    self.coverage_graph_result.append([os.path.realpath(os.path.join(self.output, 'coverage_graph_{}.pdf'.format(valid_filename(reference)))), reference.replace('_', '\_')])
                    fig, ax = plt.subplots(nrows=1, ncols=1)
                    # Change labels if there are more unbinned entries than binned entries, i.e. the binning influences the labels.
                    if len_reference <= reference_coverage.size:
                        factor = float(reference_coverage.size)/len_reference
                        self._set_axis(ax, factor)
                    ax.set_yscale('symlog')
                    ax.plot(range(len_reference), reference_coverage_mean, 'r', label='mean')
                    ax.plot(range(len_reference), reference_coverage_max, 'b', label='max')
                    ax.plot(range(len_reference), reference_coverage_min, 'g', label='min')
                    ax.legend(bbox_to_anchor=(0., 1.05, 1., .102), loc=3, ncol=3, mode="expand", borderaxespad=0.)
                    ax.set_xlabel('Position')
                    ax.set_ylabel('Coverage')
                    ax.set_title('Coverage Graph')
                    pylab.xlim([0, len_reference])
                    pylab.ylim(ymin=-0.05)
                    pdf.savefig()
                    plt.close()
        if len(self.coverage_graph_result) == 0:
            self.coverage_graph_result.append(None)

    ## Generate one pdf for each reference showing the genome position (y-axis) an the respective coverage (x-axis). The genome is reduced/binned into 1000 points.
    #  @param self The object pointer.
    #  @param reference Reference to count for.
    #  @param i Reference counter.
    def mapq_graph(self, reference, i):
        log.print_write(self.logger, 'info', '=== Mapping Quality Graph #{} ==='.format(i))
        if self.reference_mapq_avg is None:
            self.count_mapq(reference)
        # Trim coverages of 0 at the end.
        reference_mapq = np.trim_zeros(self.reference_mapq_avg, 'b')
        # If there is something left of the reference.
        if reference_mapq.size > 0:
            # Use a sliding window to create ~1000 datapoints out of the reference. 
            reference_mapq_mean = []
            reference_mapq_max = []
            reference_mapq_min = []
            window_size = min(int(reference_mapq.size/10), 50000)
            for w in window(reference_mapq, size=window_size, step=max(1, int((reference_mapq.size-window_size)/1000))):
                reference_mapq_mean.append(int(np.mean(w)))
                reference_mapq_max.append(max(w))
                reference_mapq_min.append(min(w))
            reference_mapq_mean = np.array(reference_mapq_mean)
            reference_mapq_max = np.array(reference_mapq_max)
            reference_mapq_min = np.array(reference_mapq_min)
            # Get the number of binned reference.
            len_reference = reference_mapq_mean.size
            # Plot bases per coverage as line diagram.
            if len_reference > 0:
                with PdfPages(os.path.join(self.output, 'mapq_graph_{}.pdf'.format(valid_filename(reference)))) as pdf:
                    self.mapq_graph_result.append([os.path.realpath(os.path.join(self.output, 'mapq_graph_{}.pdf'.format(valid_filename(reference)))), reference.replace('_', '\_')])
                    fig, ax = plt.subplots(nrows=1, ncols=1)
                    # Change labels if there are more unbinned entries than binned entries, i.e. the binning influences the labels.
                    if len_reference <= reference_mapq.size:
                        factor = float(reference_mapq.size)/len_reference
                        self._set_axis(ax, factor)
                    #self.ax.set_yscale('symlog')
                    ax.plot(range(len_reference), reference_mapq_mean, 'r', label='mean')
                    ax.plot(range(len_reference), reference_mapq_max, 'b', label='max')
                    ax.plot(range(len_reference), reference_mapq_min, 'g', label='min')
                    ax.legend(bbox_to_anchor=(0., 1.05, 1., .102), loc=3, ncol=3, mode="expand", borderaxespad=0.)
                    ax.set_xlabel('Position')
                    ax.set_ylabel('Mapping Quality')
                    ax.set_title('Mapping Quality Graph')
                    pylab.xlim([0, len_reference])
                    pylab.ylim(ymin=-0.1)
                    pdf.savefig()
                    plt.close()
        if len(self.mapq_graph_result) == 0:
            self.mapq_graph_result.append(None)

    ## Calculate coverage stats for one reference.
    #  @param self The object pointer.
    #  @param reference Reference to count for.
    #  @param i Reference counter.
    def coverage_stats(self, reference, i):
        log.print_write(self.logger, 'info', '=== Coverage Statistics #{} ==='.format(i))
        ## @var cov_stats
        #  Dictionary containing coverage metrics.
        self.cov_stats = dict()
        if self.reference_coverage_count is None:
            self.count_coverage(reference)
        # Trim coverages of 0 at the end.
        reference_coverage = np.trim_zeros(self.reference_coverage_count, 'b')
        # If there is something left of the reference.
        if reference_coverage.size > 0:
            mean_coverage = np.mean(reference_coverage)
            maximum_coverage = np.max(reference_coverage)
            minimum_coverage = np.min(reference_coverage)
            sd_coverage = np.std(reference_coverage)
            bases_zero_coverage = (reference_coverage == 0).sum()
            conf_int = mean_coverage + 2 * sd_coverage
            lower_conf_int = (reference_coverage < conf_int).sum()
            greater_conf_int = (reference_coverage > conf_int).sum()
            cov_stats = dict()
            cov_stats['mean'] = int(mean_coverage)
            cov_stats['max'] = int(maximum_coverage)
            cov_stats['min'] = int(minimum_coverage)
            cov_stats['sd'] = int(sd_coverage)
            cov_stats['zero'] = int(bases_zero_coverage)
            cov_stats['lower'] = int(lower_conf_int)
            cov_stats['greater'] = int(greater_conf_int)
            self.cov_stats_result.append(cov_stats)
        if len(self.cov_stats_result) == 0:
            cov_stats = dict()
            cov_stats['mean'] = -1
            cov_stats['max'] = -1
            cov_stats['min'] = -1
            cov_stats['sd'] = -1
            cov_stats['zero'] = -1
            cov_stats['lower'] = -1
            cov_stats['greater'] = -1
            self.cov_stats_result.append(cov_stats)
        with open(os.path.join(self.output, 'coverage_stats_{}.json'.format(reference)), 'w') as f:
            json.dump(self.cov_stats, f)
        #return self.cov_stats
    
    ## Read the flags of the SAM file to get metrics.
    #  @param self The object pointer.
    def read_stats(self):
        log.print_write(self.logger, 'info', '=== Read Statistics ===')
        concordant = 0
        discordant = 0
        paired = 0
        unmapped = 0
        total = 0
        mapped = 0
        #for (query, flag) in self.sam.get_flags():
        for flag in self.sam.get_flags():
            bflag = bin(flag)
            total += 1
            if flag >= 1 and bflag[-1] == '1':
                paired += 1
            if flag >= 2 and bflag[-2] == '1':
                concordant += 1
            else:
                discordant += 1
            if flag >= 4 and bflag[-3] == '1':
                unmapped += 1
        mapped = total - unmapped
        ## @var read_stats
        #  Dictionary containing read metrics.
        self.read_stats = dict()
        self.read_stats['concordant'] = concordant
        self.read_stats['discordant'] = discordant
        self.read_stats['paired'] = paired
        self.read_stats['mapped'] = mapped
        self.read_stats['unmapped'] = unmapped
        self.read_stats['total'] = total
        with open(os.path.join(self.output, 'read_stats.json'), 'w') as f:
            json.dump(self.read_stats, f)
        #self.read_stats_result.append(self.stats)
        #return self.stats

    ## Delete temporary files.
    #  @param self The object pointer.
    def cleanup(self):
        log.write(self.logger, 'info', '=== Cleaning up metrics ===')
        try:
            #for mfile in self.reads_per_quality_result+self.bases_per_coverage_result+self.mapq_graph_result+self.coverage_graph_result+[(self.sam.sam_file_path, 'bam'), (self.sam.sam_file_path[:-1] + 'i', 'bai')]:
            for mfile in [(self.sam.sam_file_path, 'bam'), (self.sam.sam_file_path[:-1] + 'i', 'bai')]:
                if mfile is not None:
                    ## @var command
                    #  Temporarly save command call.
                    self.command = 'rm {}'.format(mfile[0])
                    log.call(self.command, self.logger, shell=True)
        except sp.CalledProcessError:
            log.print_write(self.logger, 'exception', 'ERROR: CPE in metrics.cleanup()')
            return False
        self.reads_per_quality_result = []
        self.bases_per_coverage_result = []
        self.mapq_graph_result = []
        self.coverage_graph_result = []
        return True
