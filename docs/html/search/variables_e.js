var searchData=
[
  ['r2',['r2',['../classMapping_1_1mapping-4_1_1MAPPING.html#a760936794bac7a4e4a8ede91df15e357',1,'Mapping::mapping-4::MAPPING']]],
  ['read_5fcounter',['read_counter',['../classMapping_1_1mapping-4_1_1MAPPING.html#a3706b04abb909dfcba31969460de6188',1,'Mapping::mapping-4::MAPPING']]],
  ['read_5fdict',['read_dict',['../classMapping_1_1mapping-4_1_1MAPPING.html#a9174e5d674a015d018cfbcb42a09c7e5',1,'Mapping::mapping-4::MAPPING']]],
  ['read_5fstats',['read_stats',['../classMapping_1_1metrics_1_1METRICS.html#a268f52660c3109ebe3b3f1d9942c133b',1,'Mapping::metrics::METRICS']]],
  ['readgroups',['readgroups',['../classMapping_1_1mapper_1_1Bowtie2.html#a9736aecab524feb5bccb33ee3d4913c6',1,'Mapping.mapper.Bowtie2.readgroups()'],['../classMapping_1_1mapper_1_1BWAMem.html#aee945c588508482fde0f2d989d6d2b12',1,'Mapping.mapper.BWAMem.readgroups()']]],
  ['reads',['reads',['../classMapping_1_1mapping-4_1_1MAPPING.html#a9dfea62d83c6c7a22d243fc5075bfa90',1,'Mapping::mapping-4::MAPPING']]],
  ['reads_5fper_5fquality_5fresult',['reads_per_quality_result',['../classMapping_1_1metrics_1_1METRICS.html#a7131fd84cfec68543a04075b95079f6c',1,'Mapping::metrics::METRICS']]],
  ['reference',['reference',['../classMapping_1_1mapper_1_1Mapper.html#a2b7f6402fb70951ff4ce0d30a68b293c',1,'Mapping.mapper.Mapper.reference()'],['../classMapping_1_1mapper_1_1Bowtie2.html#a319eae3b764cca4014af449aa0c204d9',1,'Mapping.mapper.Bowtie2.reference()'],['../classMapping_1_1mapper_1_1BWAMem.html#a42bd39d3ddcee1f4e38dba2c1116c729',1,'Mapping.mapper.BWAMem.reference()']]],
  ['reference_5fcoverage_5fcount',['reference_coverage_count',['../classMapping_1_1metrics_1_1METRICS.html#a73df44b441fa253b1790360520a82b4c',1,'Mapping::metrics::METRICS']]],
  ['reference_5ffasta',['reference_fasta',['../classMapping_1_1mapper_1_1Bowtie2.html#a91e2ce70dac6882ba6e627c5d7dfdfc7',1,'Mapping::mapper::Bowtie2']]],
  ['reference_5fmapq_5favg',['reference_mapq_avg',['../classMapping_1_1metrics_1_1METRICS.html#ae4dace40b650e374c7dfac47181b0cb8',1,'Mapping::metrics::METRICS']]],
  ['reffinder_5fversion',['reffinder_version',['../classMapping_1_1mapping-4_1_1MAPPING.html#a8c068e75c3fee7c23dfe0121fadbc4e6',1,'Mapping::mapping-4::MAPPING']]],
  ['release',['release',['../classMapping_1_1mapping-4_1_1MAPPING.html#aeedf1b85ad19232cd050def7cfa036b5',1,'Mapping::mapping-4::MAPPING']]],
  ['report_5ffile',['report_file',['../classMapping_1_1mapping-4_1_1MAPPING.html#a7f037e238224b610a3bc8529a981b84d',1,'Mapping::mapping-4::MAPPING']]],
  ['required',['required',['../classMapping_1_1mapping-4_1_1MAPPING.html#add05bbd09ec27df470313f831328cd13',1,'Mapping::mapping-4::MAPPING']]]
];
