var searchData=
[
  ['read_5fstats',['read_stats',['../classMapping_1_1metrics_1_1METRICS.html#a67b9632e918ab11389db0f1f02f89f61',1,'Mapping::metrics::METRICS']]],
  ['reads_5fper_5fquality',['reads_per_quality',['../classMapping_1_1metrics_1_1METRICS.html#ad081e87aa2703d0d88272f9d80807f25',1,'Mapping::metrics::METRICS']]],
  ['report',['report',['../classMapping_1_1mapping-4_1_1MAPPING.html#af2e6ef3c6234d0ebec31766147f43f60',1,'Mapping.mapping-4.MAPPING.report()'],['../classMapping_1_1mapper_1_1Mapper.html#ae2eef9daec64117090888d9a2f6483bd',1,'Mapping.mapper.Mapper.report()'],['../classMapping_1_1mapper_1_1Bowtie2.html#aa9478470a7e7cc6bc871e81ae5803fd8',1,'Mapping.mapper.Bowtie2.report()'],['../classMapping_1_1mapper_1_1BWAMem.html#aaf39e3f3ef0f8998ab989f2174799ea6',1,'Mapping.mapper.BWAMem.report()']]],
  ['run',['run',['../classMapping_1_1mapping-4_1_1MAPPING.html#a17055b3757e7fe91ee326ddd9ddec9ad',1,'Mapping::mapping-4::MAPPING']]],
  ['run_5fmetrics',['run_metrics',['../classMapping_1_1mapping-4_1_1MAPPING.html#a57c890d8cbf89fbf6e0151a9af542fa5',1,'Mapping::mapping-4::MAPPING']]],
  ['run_5freffinder',['run_reffinder',['../classMapping_1_1mapping-4_1_1MAPPING.html#a79a30322cb2ea8f31adb7df4f4ff3f00',1,'Mapping::mapping-4::MAPPING']]]
];
