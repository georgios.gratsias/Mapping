import logging
import select
import sys
if sys.version_info < (3, 2):
    import subprocess32 as sp
else:
    import subprocess as sp

def call(popenargs, logger, stdout_log_level=logging.DEBUG,
         stderr_log_level=logging.ERROR, **kwargs):
    """
    Variant of subprocess.call that accepts a logger instead of stdout/stderr,
    and logs stdout/stderr messages via logger.debug.
    """
    try:
        child = sp.Popen(popenargs, stdout=sp.PIPE, stderr=sp.PIPE, **kwargs)

        log_level = {
            child.stdout: stdout_log_level,
            child.stderr: stdout_log_level} # stderr_log_level

        def check_io():
            ready_to_read = select.select(
                [child.stdout, child.stderr], [], [], 1000)[0]
            for io in ready_to_read:
                line = str(io.readline(), 'utf-8')
                if line and logger is not None:
                    logger.log(log_level[io], line[:-1])

        # keep checking stdout/stderr until the child exits
        while child.poll() is None:
            check_io()

        check_io()  # check again to catch anything after the process exits
    except Exception as e:
        raise e

    if child.returncode != 0:
        raise sp.CalledProcessError(child.returncode, popenargs, None)
    else:
        return child.wait()

def check_output(popenargs, logger, stdout_log_level=logging.DEBUG,
         stderr_log_level=logging.ERROR, **kwargs):
    """
    Variant of subprocess.check_output that accepts a logger instead of stdout/stderr,
    and logs stdout/stderr messages via logger.debug and returns the output in the same
    manner as subprocess.check_output.
    """
    
    log_stderr = True
    try:
        kwargs['stderr']
        del kwargs['stderr']
    except KeyError:
        log_stderr = False

    output = ''

    try:
        child = sp.Popen(popenargs, stdout=sp.PIPE, stderr=sp.PIPE, **kwargs)

        log_level = {
            child.stdout: stdout_log_level,
            child.stderr: stdout_log_level} #stderr_log_level

        def check_io():
            ready_to_read = select.select(
                [child.stdout, child.stderr], [], [], 1000)[0]
            for io in ready_to_read:
                line = str(io.readline(), 'utf-8')
                if line and logger is not None:
                    logger.log(log_level[io], line[:-1])
                    if log_level[io] == 'stdout_log_level':
                        return line
                    elif log_stderr:
                        return line

        # keep checking stdout/stderr until the child exits
        while child.poll() is None:
            s = check_io()
            if s is not None:
                output += s      
        s = check_io()  # check again to catch anything after the process exits
        if s is not None: 
            output += s      

    except Exception as e:
        raise e

    if child.returncode != 0:
        raise sp.CalledProcessError(child.returncode, popenargs, None)
    else:
        child.wait()
        return output

def write(logger, mode, message):
    """Write message to the log.

    Writes message to logger using log_level mode.

    Args:
        message: Message to write in the log.
        logger: Open logger instance.
        mode: Log_lvl to write lmessage to.
    """

    if logger is not None:
        if message is None:
            message = pmessage
        if mode == 'error':
            logger.error(message)
        elif mode == 'critical':
            logger.critial(message)
        elif mode == 'debug':
            logger.debug(message)
        elif mode == 'warning':
            logger.warning(message)
        elif mode == 'info':
            logger.info(message)
        elif mode == 'exception':
            logger.exception(message)
        else:
            logger.error(message)


def print_write(logger, mode, pmessage, lmessage = None):
    """Print a message to the screen and write another one to the log.

    Prints pmessage to the screen and writes lmessage to logger using
    log_level mode.

    Args:
        pmessage: Message to print to the screen.
        lmessage: Message to write in the log.
        logger: Open logger instance.
        mode: Log_lvl to write lmessage to.
    """

    print(pmessage)
    if logger is not None:
        if lmessage is None:
            lmessage = pmessage
        if mode == 'error':
            logger.error(lmessage)
        elif mode == 'critical':
            logger.critial(lmessage)
        elif mode == 'debug':
            logger.debug(lmessage)
        elif mode == 'warning':
            logger.warning(lmessage)
        elif mode == 'info':
            logger.info(lmessage)
        elif mode == 'exception':
            logger.exception(lmessage)
        else:
            logger.error(lmessage)
